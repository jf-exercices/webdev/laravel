<?php

// Bien que nous voyons les tests dans Laravel, ils fonctionnent selon le même principe dans Symfony
// Les deux Frameworks basent leurs classes de test sur PHPUnit, avec donc un grand nombre de méthodes communes.
// Dans les deux cas, il convient de configurer un environnement de test
// Cela peut se faire en créant un .env.test et en modifiant le fichier phpunit.xml

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
// ne pas oublier de use les classes utiles
use App\Models\User;
use App\Helpers\Helpers;

class UserTest extends TestCase
{
    // indispensable pour éviter de conserver les données en DB lors des tests
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_getUserOne(): void
    {
        // 1 ne fonctionnera que si la DB est vide.
        // Prévoir de récupérer l'autoincrement ou alors d'utiliser une DB de test (.env)
        $id = 1;

        // generate one user in User table
        $user = User::factory()->count(1)->create();
        // Laravel possède des outils spécifiques comme Mock. Faker peut également être utilisé. (valide pour Symfony)

        // assert user creation (insert)
        $this->assertTrue($user->contains('id', $id));

        // assert route HTTP status
        $response = $this->get('user/' . $id);

        $response->assertStatus(200);

        // assert Content (from JSON)
        $data = json_decode($response->getContent(), true);

        // remove unused fields
        $userModel = new User();
        $cols = Helpers::unsetByValue($userModel->getTableColumns(), ['password', 'remember_token']);

        // assert each field from User table from Content
        foreach ($cols as $key => $value) {
            $this->assertArrayHasKey($value, $data);
        }
    }

    public function test_getUserNone(): void
    {
        $response = $this->get('user');

        // le code HTTP 404 c'est bien, mais prévoir une vue gérant les erreurs, c'est mieux !
        $response->assertStatus(404);
    }

    public function test_getUserNoneWithSlash(): void
    {
        $response = $this->get('user/');

        // le code HTTP 404 c'est bien, mais prévoir une vue gérant les erreurs, c'est mieux !
        $response->assertStatus(404);
        // On pourrait utiliser une redirection vers une route spécifique et la tester
        // $response->assertRedirectToRoute('error/http', 302);
    }

    public function test_getUserNotANumber(): void
    {
        $response = $this->get('user/a');

        // si la réponse HTTP retourne une 200, c'est qu'il manque une vérification du type de paramètre dans la route !
        $response->assertStatus(200);
    }

    public function test_adminExists(): void
    {
        // Bon nombre de méthodes assertDatabase... existent afin de tester le contenu de la DB
        $this->assertDatabaseHas('users', [
            'admin' => 1,
        ]);
    }

    // Laravel possède également d'autres outils de tests comme :
    // DUSK pour tester vos routes et API : https://laravel.com/docs/10.x/dusk
}
