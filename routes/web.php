<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/{id}', [
    \App\Http\Controllers\UserController::class,'get'
]);

Route::post('/user/update/{id}', [
    \App\Http\Controllers\UserController::class,'update'
]);

Route::get('/user/update2', '\App\Http\Controllers\User@update');
