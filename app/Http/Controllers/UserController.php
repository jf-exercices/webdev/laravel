<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{

    public function get(Request $request)
    {
        return User::find($request->id);
    }

    public function update(Request $request): RedirectResponse {
        $user = new User();

        $user = $user->find($request->id);

        $user->name = $request->name;

        $user->save();

        return redirect('/user.update.validation');
    }
}
