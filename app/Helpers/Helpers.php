<?php

namespace App\Helpers;

class Helpers
{
    public static function unsetByValue(array $array, array $values) : array
    {
        foreach ($values as $value) {
            if (($key = array_search($value, $array)) !== false) {
                unset($array[$key]);
            }
        }
        return $array;
    }
}
